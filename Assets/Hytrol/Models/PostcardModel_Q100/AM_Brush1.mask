%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: AM_Brush1
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Q100_sorter_base
    m_Weight: 1
  - m_Path: Q100_sorter_base/Q100_sorter_Brush_01
    m_Weight: 0
  - m_Path: Q100_sorter_base/Q100_sorter_Brush_02
    m_Weight: 0
  - m_Path: Q100_sorter_base/Q100_sorter_Brush_03
    m_Weight: 1
  - m_Path: Q100_sorter_base/Q100_sorter_Brush_04
    m_Weight: 1
  - m_Path: Q100_sorter_base/Q100_sorter_Lever_01
    m_Weight: 0
  - m_Path: Q100_sorter_base/Q100_sorter_Lever_01/Q100_sorter_Door_01
    m_Weight: 0
  - m_Path: Q100_sorter_base/Q100_sorter_Lever_02
    m_Weight: 1
  - m_Path: Q100_sorter_base/Q100_sorter_Lever_02/Q100_sorter_Door_02
    m_Weight: 1
  - m_Path: Q100_sorter_base/Q100_sorter_Lever_03
    m_Weight: 0
  - m_Path: Q100_sorter_base/Q100_sorter_Lever_03/Q100_sorter_Door_03
    m_Weight: 0
  - m_Path: Q100_sorter_base/Q100_sorter_Lever_04
    m_Weight: 1
  - m_Path: Q100_sorter_base/Q100_sorter_Lever_04/Q100_sorter_Door_04
    m_Weight: 1
  - m_Path: Srf1_Q100
    m_Weight: 1
