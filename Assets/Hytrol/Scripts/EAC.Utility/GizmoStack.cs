
namespace EAC.Utility
{

// system namespaces
using System.Collections.Generic;
// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;

public static class GizmosStack
{
    private struct StackElement
    {
        public Color color;
        public Matrix4x4 matrix;

        public StackElement (Color inColor, Matrix4x4 inMatrix)
        {
            color = inColor;
            matrix = inMatrix;
        }
    }

    private static Stack<StackElement> m_stack = new Stack<StackElement>();

    public static void SaveState ()
    {
        m_stack.Push(new StackElement(Gizmos.color, Gizmos.matrix));
    }

    public static void SaveState (Matrix4x4 newMatrix, Color newColor)
    {
        m_stack.Push(new StackElement(Gizmos.color, Gizmos.matrix));
        Gizmos.matrix = newMatrix;
        Gizmos.color = newColor;
    }

    public static void RestoreState ()
    {
        Assert.IsTrue(m_stack.Count > 0);

        StackElement topElement = m_stack.Pop();
        Gizmos.matrix = topElement.matrix;
        Gizmos.color = topElement.color;
    }
}

} // namespace EAC.Utility


