
namespace EAC.Hytrol.Constants
{

// unity namespaces
using UnityEngine;

public static class Layers
{
    // unity layers
    public const string nameDefault = "Default";
    public const string nameTransparentFX = "TransparentFX";
    public const string nameIgnoreRaycast = "Ignore Raycast";
    public const string nameWater = "Water";
    public const string nameUI = "UI";

    public static readonly int maskDefault = LayerMask.GetMask(nameDefault);
    public static readonly int maskTransparentFX = LayerMask.GetMask(nameTransparentFX);
    public static readonly int maskIgnoreRaycast = LayerMask.GetMask(nameIgnoreRaycast);
    public static readonly int maskWater = LayerMask.GetMask(nameWater);
    public static readonly int maskUI = LayerMask.GetMask(nameUI);

    public static readonly int layerDefault = LayerMask.NameToLayer(nameDefault);
    public static readonly int layerTransparentFX = LayerMask.NameToLayer(nameTransparentFX);
    public static readonly int layerIgnoreRaycast = LayerMask.NameToLayer(nameIgnoreRaycast);
    public static readonly int layerWater = LayerMask.NameToLayer(nameWater);
    public static readonly int layerUI = LayerMask.NameToLayer(nameUI);

    // application layers
    // public const string nameInteractive = "Interactive";

    // public static readonly int maskInteractive = LayerMask.GetMask(nameInteractive);

    // public static readonly int layerInteractive = LayerMask.NameToLayer(nameInteractive);
}

} // namespace EAC.Hytrol.Constants
