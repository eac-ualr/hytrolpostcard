
namespace EAC.Hytrol.Constants
{

public static class Tags
{
    // unity tags
    public const string respawn = "Respawn";
    public const string finish = "Finish";
    public const string editorOnly = "EditorOnly";
    public const string mainCamera = "MainCamera";
    public const string player = "Player";
    public const string gameController = "GameController";

    // application tags
    public const string brush = "Brush";
}

} // namespace EAC.Hytrol.Constants
