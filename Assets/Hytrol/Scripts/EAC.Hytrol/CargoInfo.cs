
namespace EAC.Hytrol
{

// system namespaces
using NonSerializedAttribute = System.NonSerializedAttribute;
using System.Collections;
// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;
// local namespaces
using Tags = EAC.Hytrol.Constants.Tags;

public class CargoInfo
    : MonoBehaviour
{
    // ----------------------------------------------------------------------
    #region Types

    public delegate void BrushEvent(CargoInfo info);

    #endregion Types
    // ----------------------------------------------------------------------

    public event BrushEvent onBrushCollision;

    public SortingBin sortingBin;

    [NonSerialized]
    public Conveyor conveyor;
    [NonSerialized]
    public Transform cargoTransform;
    [NonSerialized]
    public float cargoPosition;
    [NonSerialized]
    public bool brushTriggered;
    [NonSerialized]
    public CargoSpawnManager cargoManager;

    private Collider m_collider;
    private Rigidbody m_rigidBody;
    private Coroutine m_destroySelfCoro;

    private const float kDestroyTime = 10.0f;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        conveyor = null;
        cargoTransform = GetComponent<Transform>();
        m_collider = GetComponent<Collider>();
        Assert.IsNotNull(m_collider);
        m_rigidBody = GetComponent<Rigidbody>();
        Assert.IsNotNull(m_rigidBody);

        brushTriggered = false;

        m_collider.isTrigger = true;
        m_rigidBody.isKinematic = true;
        m_destroySelfCoro = null;
    }

    protected void OnTriggerEnter (Collider collider)
    {
        if (collider.gameObject.CompareTag(Tags.brush))
        {
            // sweep sorter brush hit cargo, make it physics controlled and mark it for
            // destruction after a timeout (see DestroySelfCoro)
            m_collider.isTrigger = false;
            m_rigidBody.isKinematic = false;
            EmitBrushCollision();

            // launch coroutine to destroy this object after timeout
            if (m_destroySelfCoro == null)
            {
                m_destroySelfCoro = StartCoroutine(DestroySelfCoro());
            }
        }
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void SetVisible (bool visible)
    {
        Renderer[] renderers = GetComponentsInChildren<Renderer>(true);

        foreach (Renderer renderer in renderers)
        {
            renderer.enabled = visible;
        }
    }

    // ----------------------------------------------------------------------

    private void EmitBrushCollision ()
    {
        BrushEvent onBrushCollisionEvent = onBrushCollision;

        if (onBrushCollisionEvent != null)
        {
            onBrushCollisionEvent(this);
        }
    }

    private IEnumerator DestroySelfCoro ()
    {
        yield return new WaitForSeconds(kDestroyTime);

        cargoManager.NotifyCargoDestroyed(this);

        Destroy(this.gameObject);
    }
}

} // namespace EAC.Hytrol
