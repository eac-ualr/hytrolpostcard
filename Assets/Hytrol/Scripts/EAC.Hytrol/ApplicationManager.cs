
namespace EAC.Hytrol
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;

public class ApplicationManager
    : MonoBehaviour
{
    public enum Mode
    {
        TrackAugmentation,
        DisplayInstructions,
    }

    public delegate void ModeChangedEvent(Mode oldMode, Mode newMode);

    public static readonly string objectName = "ApplicationManager";

    private Mode m_mode;
    private Mode m_prevMode;

    // ----------------------------------------------------------------------

    public event ModeChangedEvent onModeChanged;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        m_mode = Mode.DisplayInstructions;
        m_prevMode = Mode.DisplayInstructions;
    }

    protected void Start ()
    {
        DoSetMode(Mode.DisplayInstructions);
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void SwitchMode (Mode newMode)
    {
        if (newMode != m_mode)
        {
            DoSetMode(newMode);
        }
    }

    // ----------------------------------------------------------------------

    private void EmitModeChanged (Mode oldMode, Mode newMode)
    {
        ModeChangedEvent onModeChangedEvent = onModeChanged;

        if (onModeChangedEvent != null)
        {
            onModeChangedEvent(oldMode, newMode);
        }
    }

    private void DoSetMode (Mode newMode)
    {
        m_prevMode = m_mode;
        m_mode = newMode;

        EmitModeChanged(m_prevMode, m_mode);
    }
}

} // namespace EAC.Hytrol
