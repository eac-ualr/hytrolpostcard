
namespace EAC.Hytrol
{

// unity namespaces
using UnityEngine;
// external namespaces
using Vuforia;


public class SweepSorterTrackableEventHandler
    : MonoBehaviour
    , ITrackableEventHandler
{
    // ----------------------------------------------------------------------
    #region Types

    public delegate void TrackingStateEvent(bool found);

    #endregion Types
    // ----------------------------------------------------------------------

    public event TrackingStateEvent onTrackingStateChanged;

    [Tooltip("Trackable objects this handler listens to")]
    public TrackableBehaviour[] trackableBehaviours;

    private int m_trackedCount = 0;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void OnEnable ()
    {
        for (int i = 0, iEnd = trackableBehaviours.Length; i < iEnd; ++i)
        {
            trackableBehaviours[i].RegisterTrackableEventHandler(this);
        }
    }

    protected void OnDisable ()
    {
        for (int i = 0, iEnd = trackableBehaviours.Length; i < iEnd; ++i)
        {
            trackableBehaviours[i].UnregisterTrackableEventHandler(this);
        }
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------
    #region ITrackableEventHandler

    public void OnTrackableStateChanged (
        TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        // count number of trackable objects that are tracked now
        int newTrackedCount = 0;

        for (int i = 0, iEnd = trackableBehaviours.Length; i < iEnd; ++i)
        {
            if (trackableBehaviours[i].CurrentStatus == TrackableBehaviour.Status.DETECTED
                || trackableBehaviours[i].CurrentStatus == TrackableBehaviour.Status.TRACKED
                || trackableBehaviours[i].CurrentStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                newTrackedCount += 1;
            }
        }

        if (m_trackedCount == 0 && newTrackedCount > 0)
        {
            OnTrackingFound();
        }
        else if (m_trackedCount > 0 && newTrackedCount == 0)
        {
            OnTrackingLost();
        }

        m_trackedCount = newTrackedCount;
    }

    #endregion ITrackableEventHandler
    // ----------------------------------------------------------------------

    private void OnTrackingFound ()
    {
        EmitTrackingStateChanged(true);
    }

    private void OnTrackingLost ()
    {
        EmitTrackingStateChanged(false);
    }

    private void EmitTrackingStateChanged (bool tracking)
    {
        TrackingStateEvent trackingStateEvent = onTrackingStateChanged;

        if (trackingStateEvent != null)
        {
            trackingStateEvent(tracking);
        }
    }
}

} // namespace EAC.Hytrol
