
namespace EAC.Hytrol
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;
// local namespaces
using ObjectFinder = EAC.Utility.ObjectFinder;

public class CargoSpawnUI
    : MonoBehaviour
{
    public CargoSpawnManager spawnManager;

    private ApplicationManager m_applicationManager;
    private GameObject m_panelButtonsGO;
    private Toggle m_toggleAutoSpawn;
    private Button m_btnSpawnCargo;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        Assert.IsNotNull(spawnManager);

        m_applicationManager
            = ObjectFinder.FindObjectComponent<ApplicationManager>(ApplicationManager.objectName);
        Assert.IsNotNull(m_applicationManager);

        m_panelButtonsGO = ObjectFinder.FindChild(gameObject, "Panel_Buttons_Conveyor");
        Assert.IsNotNull(m_panelButtonsGO);

        m_toggleAutoSpawn = ObjectFinder.GetChildComponent<Toggle>(gameObject, "Toggle_AutoSpawn");
        Assert.IsNotNull(m_toggleAutoSpawn);

        m_btnSpawnCargo = ObjectFinder.GetChildComponent<Button>(gameObject, "Button_SpawnCargo");
        Assert.IsNotNull(m_btnSpawnCargo);

        if (m_applicationManager != null)
        {
            m_applicationManager.onModeChanged += OnApplicationModeChanged;
        }
    }

    protected void Start ()
    {
        SetAutoSpawnEnabled(m_toggleAutoSpawn.isOn);
    }

    protected void OnEnable ()
    {
        if (spawnManager != null)
        {
            spawnManager.onCargoSpawnReady += OnCargoSpawnReady;
        }
    }

    protected void OnDisable ()
    {
        if (spawnManager != null)
        {
            spawnManager.onCargoSpawnReady -= OnCargoSpawnReady;
        }
    }

    protected void OnDestroy ()
    {
        if (m_applicationManager != null)
        {
            m_applicationManager.onModeChanged -= OnApplicationModeChanged;
        }
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------
    #region UI Event Handlers

    public void OnValueChangedToggleAutoSpawn (bool toggleValue)
    {
        SetAutoSpawnEnabled(toggleValue);
    }

    public void OnClickButtonSpawnCargo ()
    {
        spawnManager.SpawnRandomCargo();
    }

    #endregion UI Event Handlers
    // ----------------------------------------------------------------------

    private void OnCargoSpawnReady (CargoSpawnManager spawnManager, bool ready)
    {
        SetButtonsInteractable(ready);
    }

    private void SetButtonsInteractable (bool interactable)
    {
        m_btnSpawnCargo.interactable = interactable;
    }

    private void SetAutoSpawnEnabled (bool enabled)
    {
        SetButtonsInteractable(!enabled);
        spawnManager.SetAutoSpawnEnabled(enabled);
    }

    private void OnApplicationModeChanged (
        ApplicationManager.Mode oldMode, ApplicationManager.Mode newMode)
    {
        if (newMode == ApplicationManager.Mode.TrackAugmentation)
        {
            m_panelButtonsGO.SetActive(false);
            //m_panelButtonsGO.SetActive(true);
        }
        else
        {
            m_panelButtonsGO.SetActive(false);
        }
    }
}

} // namespace EAC.Hytrol
