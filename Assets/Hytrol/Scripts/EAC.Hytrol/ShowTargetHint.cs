
namespace EAC.Hytrol
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;

public class ShowTargetHint
    : MonoBehaviour
{
    public SweepSorterTrackableEventHandler eventHandler;

    private Animator m_animator;
    private float m_lastTrackedTime;
    private bool m_tracking;
    private int m_hashParamVisible;

    private const float kShowTargetHintDelay = 5.0f;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        Assert.IsNotNull(eventHandler);

        m_animator = GetComponent<Animator>();
        Assert.IsNotNull(m_animator);

        m_lastTrackedTime = Time.time;
        m_tracking = false;
        m_hashParamVisible = Animator.StringToHash("Visible");
    }

    protected void Update ()
    {
        if (!m_tracking && Time.time > m_lastTrackedTime + kShowTargetHintDelay)
        {
            if (m_animator.GetBool(m_hashParamVisible) == false)
                m_animator.SetBool(m_hashParamVisible, true);
        }
    }

    protected void OnEnable ()
    {
        if (eventHandler != null)
        {
            eventHandler.onTrackingStateChanged += OnTrackingStateChanged;
        }
    }

    protected void OnDisable ()
    {
        if (eventHandler != null)
        {
            eventHandler.onTrackingStateChanged -= OnTrackingStateChanged;
        }
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    private void OnTrackingStateChanged (bool found)
    {
        if (found)
        {
            // hide hint
            m_animator.SetBool(m_hashParamVisible, false);
            m_tracking = true;
        }
        else
        {
            m_lastTrackedTime = Time.time;
            m_tracking = false;
        }
    }
}

} // namespace EAC.Hytrol
