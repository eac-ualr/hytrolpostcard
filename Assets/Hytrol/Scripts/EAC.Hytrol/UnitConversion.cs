
namespace EAC.Hytrol
{

public static class UnitConversion
{
    // converts speed from feet/minute to meter/second
    public static float FPM_2_MPS (float speedFPM)
    {
        return speedFPM * 0.00508f;
    }

    // converts speed from meter/second to feet/minute
    public static float MPS_2_FPM (float speedMPS)
    {
        return speedMPS * 196.85f;
    }
}

} // namespace EAC.Hytrol
