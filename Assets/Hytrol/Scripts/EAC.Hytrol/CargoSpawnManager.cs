
namespace EAC.Hytrol
{

// system namespaces
using System.Collections.Generic;
using System.Collections;
// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;

public class CargoSpawnManager
    : MonoBehaviour
{
    // ----------------------------------------------------------------------
    #region Types

    public delegate void SpawnReadyEvent(CargoSpawnManager spawnManager, bool ready);

    public delegate void SpawnEvent(CargoSpawnManager spawnManager, GameObject cargoGO);

    #endregion Types
    // ----------------------------------------------------------------------

    public GameObject[] cargoPrefabs;
    public Conveyor conveyor;

    public event SpawnReadyEvent onCargoSpawnReady;
    public event SpawnEvent onCargoSpawn;

    private List<CargoInfo> m_cargos;
    private SweepSorterTrackableEventHandler m_eventHandler;
    private Coroutine m_spawnDelayCoro;
    private Coroutine m_autoSpawnCoro;
    private bool m_visible;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        Assert.IsNotNull(conveyor);
        m_eventHandler = FindObjectOfType<SweepSorterTrackableEventHandler>();
        Assert.IsNotNull(m_eventHandler);

        m_cargos = new List<CargoInfo>();
        m_visible = true;
    }

    protected void Start ()
    {
        EmitCargoSpawnReady(true);
    }

    protected void OnEnable ()
    {
        if (m_eventHandler != null)
        {
            m_eventHandler.onTrackingStateChanged += OnTrackingStateChanged;
        }
    }

    protected void OnDisable ()
    {
        if (m_eventHandler != null)
        {
            m_eventHandler.onTrackingStateChanged -= OnTrackingStateChanged;
        }
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void SpawnCargo (int cargoIdx)
    {
        Assert.IsTrue(0 <= cargoIdx && cargoIdx < cargoPrefabs.Length);

        if (m_spawnDelayCoro != null)
        {
            Debug.LogErrorFormat("[CargoSpawnManager.SpawnCargo] SpawnDelay violation!");
            return;
        }

        DoSpawnCargo(cargoIdx);
    }

    public void SpawnRandomCargo ()
    {
        if (m_spawnDelayCoro != null)
        {
            Debug.LogErrorFormat("[CargoSpawnManager.SpawnCargo] SpawnDelay violation!");
            return;
        }

        int cargoIdx = Random.Range(0, cargoPrefabs.Length);
        DoSpawnCargo(cargoIdx);
    }

    public void SetAutoSpawnEnabled (bool enabled)
    {
        if (enabled)
        {
            if (m_autoSpawnCoro != null)
            {
                Debug.LogWarningFormat(
                    "[CargoSpawnManager.SetAutoSpawnEnabled] Auto spawn already enabled!");
                return;
            }

            if (m_spawnDelayCoro != null)
            {
                StopCoroutine(m_spawnDelayCoro);
                m_spawnDelayCoro = null;
            }

            m_autoSpawnCoro = StartCoroutine(AutoSpawnCoro());
        }
        else
        {
            if (m_autoSpawnCoro == null)
            {
                Debug.LogWarningFormat(
                    "[CargoSpawnManager.SetAutoSpawnEnabled] Auto spawn not enabled!");
                return;
            }

            StopCoroutine(m_autoSpawnCoro);
            m_autoSpawnCoro = null;

            if (m_spawnDelayCoro == null)
            {
                m_spawnDelayCoro = StartCoroutine(SpawnDelayCoro(2.0f));
            }
        }
    }

    public void NotifyCargoDestroyed (CargoInfo info)
    {
        m_cargos.Remove(info);
    }

    // ----------------------------------------------------------------------

    private void EmitCargoSpawnReady (bool ready)
    {
        SpawnReadyEvent onCargoSpawnReadyEvent = onCargoSpawnReady;

        if (onCargoSpawnReadyEvent != null)
        {
            onCargoSpawnReadyEvent(this, ready);
        }
    }

    private void EmitCargoSpawn (GameObject cargoGO)
    {
        SpawnEvent onCargoSpawnEvent = onCargoSpawn;

        if (onCargoSpawnEvent != null)
        {
            onCargoSpawnEvent(this, cargoGO);
        }
    }

    private void OnTrackingStateChanged (bool found)
    {
        m_visible = found;

        for (int i = 0, iEnd = m_cargos.Count; i < iEnd; ++i)
        {
            m_cargos[i].SetVisible(found);
        }
    }

    private void DoSpawnCargo (int cargoIdx)
    {
        DoSpawnCargoCommon(cargoIdx);

        m_spawnDelayCoro = StartCoroutine(SpawnDelayCoro(2.0f));
    }

    private void DoSpawnCargoCommon (int cargoIdx)
    {
        Transform conveyorTransform = conveyor.GetComponent<Transform>();
        Vector3 spawnPosition = Vector3.zero;
        Quaternion spawnRotation = transform.rotation;

        GameObject prefab = cargoPrefabs[cargoIdx];
        GameObject cargoGO
            = GameObject.Instantiate(prefab, spawnPosition, spawnRotation, conveyorTransform);
        CargoInfo info = cargoGO.GetComponent<CargoInfo>();
        info.SetVisible(m_visible);
        info.cargoManager = this;
        conveyor.AddCargo(info);

        m_cargos.Add(info);

        EmitCargoSpawn(cargoGO);
    }

    private IEnumerator SpawnDelayCoro (float delayTime)
    {
        EmitCargoSpawnReady(false);

        yield return new WaitForSeconds(delayTime);

        EmitCargoSpawnReady(true);
        m_spawnDelayCoro = null;
    }

    private IEnumerator AutoSpawnCoro ()
    {
        EmitCargoSpawnReady(false);

        while (true)
        {
            int cargoIdx = Random.Range(0, cargoPrefabs.Length);
            DoSpawnCargoCommon(cargoIdx);

            yield return new WaitForSeconds(2.5f);
        }
    }
}

} // namespace EAC.Hytrol
