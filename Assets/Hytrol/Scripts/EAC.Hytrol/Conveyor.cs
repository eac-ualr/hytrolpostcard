
namespace EAC.Hytrol
{

// system namespaces
using System.Collections.Generic;
// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;
// local namespaces
using GizmosStack = EAC.Utility.GizmosStack;

public class Conveyor
    : MonoBehaviour
{
    [Tooltip("Speed of conveyor (FPM: feet/minute)")]
    public float speed = 150.0f;
    [Tooltip("Length of the conveyor (meter)")]
    public float length;
    [Tooltip("Length to trigger brush 1 (meter)")]
    public float lengthTriggerBrush1;
    [Tooltip("Length to trigger brush 2 (meter)")]
    public float lengthTriggerBrush2;

    public SweepSorterAnimation sorterAnimation;

    private List<CargoInfo> m_cargos;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        Assert.IsNotNull(sorterAnimation);

        m_cargos = new List<CargoInfo>();
    }

    protected void Update ()
    {
        float speedMPS = UnitConversion.FPM_2_MPS(speed);

        for (int i = 0; i < m_cargos.Count;)
        {
            float deltaT = Time.deltaTime;
            CargoInfo info = m_cargos[i];

            if (MoveCargo(info, speedMPS, deltaT))
            {
                ++i;
            }
            else
            {
                m_cargos.RemoveAt(i);
            }
        }
    }

    protected void OnDrawGizmos ()
    {
        GizmosStack.SaveState(this.transform.localToWorldMatrix, Color.blue);

        Vector3 pStart = Vector3.zero;
        Vector3 pEnd = new Vector3(0.0f, 0.0f, length);
        Gizmos.DrawLine(pStart, pEnd);

        Vector3 pBrush1 = new Vector3(0.0f, 0.0f, lengthTriggerBrush1);
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(pBrush1, 0.05f * Vector3.one);

        Vector3 pBrush2 = new Vector3(0.0f, 0.0f, lengthTriggerBrush2);
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(pBrush2, 0.05f * Vector3.one);

        GizmosStack.RestoreState();
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void AddCargo (CargoInfo info)
    {
        m_cargos.Add(info);

        info.onBrushCollision += OnBrushCollision;
        UpdateCargoPosition(info, 0.0f);
    }

    // ----------------------------------------------------------------------

    private bool MoveCargo (CargoInfo info, float speedMPS, float deltaT)
    {
        bool keepCargo = true;
        float newCargoPosition = info.cargoPosition + deltaT * speedMPS;

        if (newCargoPosition > length)
        {
            // cargo moved off end of conveyor!
            Destroy(info.gameObject);
            keepCargo = false;
        }
        else if (SortingBinFuncs.IsBrush1(info.sortingBin)
            && newCargoPosition > lengthTriggerBrush1
            && info.brushTriggered == false)
        {
            info.brushTriggered = true;
            sorterAnimation.TriggerAnimation(info.sortingBin);
            UpdateCargoPosition(info, newCargoPosition);
        }
        else if (SortingBinFuncs.IsBrush2(info.sortingBin)
            && newCargoPosition > lengthTriggerBrush2
            && info.brushTriggered == false)
        {
            info.brushTriggered = true;
            sorterAnimation.TriggerAnimation(info.sortingBin);
            UpdateCargoPosition(info, newCargoPosition);
        }
        else
        {
            UpdateCargoPosition(info, newCargoPosition);
        }

        return keepCargo;
    }

    private void UpdateCargoPosition (CargoInfo info, float newCargoPosition)
    {
        Vector3 p = transform.TransformPoint(new Vector3(0.0f, 0.0f, newCargoPosition));
        info.cargoTransform.position = p;
        info.cargoPosition = newCargoPosition;
    }

    private void OnBrushCollision (CargoInfo info)
    {
        // sweep sorter brush hit cargo, remove it from conveyor
        m_cargos.Remove(info);
        info.onBrushCollision -= OnBrushCollision;
    }
}

} // namespace EAC.Hytrol
