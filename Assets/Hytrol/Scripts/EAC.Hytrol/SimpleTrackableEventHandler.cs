
namespace EAC.Hytrol
{

// system namespaces
using System.Collections.Generic;
// unity namespaces
using UnityEngine;
using UnityEngine.Assertions;
// external namespaces
using Vuforia;


public class SimpleTrackableEventHandler
    : MonoBehaviour
    , ITrackableEventHandler
{
    private TrackableBehaviour m_trackableBehaviour = null;
    private List<Renderer> m_renderers = new List<Renderer>();

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        m_trackableBehaviour = GetComponent<TrackableBehaviour>();
        Assert.IsNotNull(m_trackableBehaviour);

        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        m_renderers.AddRange(renderers);
    }

    protected void OnEnable ()
    {
        m_trackableBehaviour.RegisterTrackableEventHandler(this);
    }

    protected void OnDisable ()
    {
        m_trackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------
    #region ITrackableEventHandler

    public void OnTrackableStateChanged (
        TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED
            || newStatus == TrackableBehaviour.Status.TRACKED
            || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED
            && newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            OnTrackingLost();
        }
    }

    #endregion ITrackableEventHandler
    // ----------------------------------------------------------------------

    private void OnTrackingFound ()
    {
        for (int i = 0, iEnd = m_renderers.Count; i < iEnd; ++i)
        {
            m_renderers[i].enabled = true;
        }
    }

    private void OnTrackingLost ()
    {
        for (int i = 0, iEnd = m_renderers.Count; i < iEnd; ++i)
        {
            m_renderers[i].enabled = false;
        }
    }
}

} // namespace EAC.Hytrol
