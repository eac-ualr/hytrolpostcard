
namespace EAC.Hytrol
{

public enum SortingBin
{
    Brush1_Left_Top,
    Brush1_Left_Bottom,
    Brush1_Right_Top,
    Brush1_Right_Bottom,

    Brush2_Left_Top,
    Brush2_Left_Bottom,
    Brush2_Right_Top,
    Brush2_Right_Bottom,
}

public static class SortingBinFuncs
{
    public static bool IsBrush1 (SortingBin sortingBin)
    {
        switch (sortingBin)
        {
        case SortingBin.Brush1_Left_Top:
        case SortingBin.Brush1_Left_Bottom:
        case SortingBin.Brush1_Right_Top:
        case SortingBin.Brush1_Right_Bottom:
            return true;
        }

        return false;
    }

    public static bool IsBrush2 (SortingBin sortingBin)
    {
        switch (sortingBin)
        {
        case SortingBin.Brush2_Left_Top:
        case SortingBin.Brush2_Left_Bottom:
        case SortingBin.Brush2_Right_Top:
        case SortingBin.Brush2_Right_Bottom:
            return true;
        }

        return false;
    }

    public static bool IsLeft (SortingBin sortingBin)
    {
        switch (sortingBin)
        {
        case SortingBin.Brush1_Left_Top:
        case SortingBin.Brush1_Left_Bottom:
        case SortingBin.Brush2_Left_Top:
        case SortingBin.Brush2_Left_Bottom:
            return true;
        }

        return false;
    }

    public static bool IsRight (SortingBin sortingBin)
    {
        switch (sortingBin)
        {
        case SortingBin.Brush1_Right_Top:
        case SortingBin.Brush1_Right_Bottom:
        case SortingBin.Brush2_Right_Top:
        case SortingBin.Brush2_Right_Bottom:
            return true;
        }

        return false;
    }
}

} // namespace EAC.Hytrol
