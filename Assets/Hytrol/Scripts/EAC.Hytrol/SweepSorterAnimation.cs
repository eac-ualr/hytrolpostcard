
namespace EAC.Hytrol
{

// unity namespaces
using UnityEngine;

public class SweepSorterAnimation
    : MonoBehaviour
{
    private Animator m_animator;
    private int m_hashBrush1LeftTop;
    private int m_hashBrush1LeftBottom;
    private int m_hashBrush1RightTop;
    private int m_hashBrush1RightBottom;
    private int m_hashBrush2LeftTop;
    private int m_hashBrush2LeftBottom;
    private int m_hashBrush2RightTop;
    private int m_hashBrush2RightBottom;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        m_animator = GetComponent<Animator>();
        m_hashBrush1LeftTop = Animator.StringToHash("Brush1_Left_Top");
        m_hashBrush1LeftBottom = Animator.StringToHash("Brush1_Left_Bottom");
        m_hashBrush1RightTop = Animator.StringToHash("Brush1_Right_Top");
        m_hashBrush1RightBottom = Animator.StringToHash("Brush1_Right_Bottom");

        m_hashBrush2LeftTop = Animator.StringToHash("Brush2_Left_Top");
        m_hashBrush2LeftBottom = Animator.StringToHash("Brush2_Left_Bottom");
        m_hashBrush2RightTop = Animator.StringToHash("Brush2_Right_Top");
        m_hashBrush2RightBottom = Animator.StringToHash("Brush2_Right_Bottom");
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void TriggerAnimation (SortingBin sortingBin)
    {
        switch (sortingBin)
        {
        case SortingBin.Brush1_Left_Top:
            m_animator.SetTrigger(m_hashBrush1LeftTop);
            break;

        case SortingBin.Brush1_Left_Bottom:
            m_animator.SetTrigger(m_hashBrush1LeftBottom);
            break;

        case SortingBin.Brush1_Right_Top:
            m_animator.SetTrigger(m_hashBrush1RightTop);
            break;

        case SortingBin.Brush1_Right_Bottom:
            m_animator.SetTrigger(m_hashBrush1RightBottom);
            break;

        case SortingBin.Brush2_Left_Top:
            m_animator.SetTrigger(m_hashBrush2LeftTop);
            break;

        case SortingBin.Brush2_Left_Bottom:
            m_animator.SetTrigger(m_hashBrush2LeftBottom);
            break;

        case SortingBin.Brush2_Right_Top:
            m_animator.SetTrigger(m_hashBrush2RightTop);
            break;

        case SortingBin.Brush2_Right_Bottom:
            m_animator.SetTrigger(m_hashBrush2RightBottom);
            break;
        }
    }
}

} // namespace EAC.Hytrol
