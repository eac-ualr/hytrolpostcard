
namespace EAC.Hytrol
{

// system namespaces
using IEnumerator = System.Collections.IEnumerator;
// unity namespaces
using UnityEngine.Assertions;
using UnityEngine;

public class BeltAnimation
    : MonoBehaviour
{
    public Renderer beltRenderer;
    public string materialName;
    public float beltSpeed;

    private int m_mainTextureID;
    private Coroutine m_animCoro;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        Assert.IsNotNull(beltRenderer);
        Assert.IsFalse(string.IsNullOrEmpty(materialName));

        m_mainTextureID = Shader.PropertyToID("_MainTex");
        m_animCoro = null;
    }

    protected void OnEnable ()
    {
        if (m_animCoro == null)
        {
            m_animCoro = StartCoroutine(AnimateBelt());
        }
    }

    protected void OnDisable ()
    {
        if (m_animCoro != null)
        {
            StopCoroutine(m_animCoro);
            m_animCoro = null;
        }
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    private IEnumerator AnimateBelt ()
    {
        Material material = FindAnimationMaterial();
        Assert.IsNotNull(material);

        Vector2 textureOffset = material.GetTextureOffset(m_mainTextureID);

        while (true)
        {
            yield return null;

            textureOffset.y += beltSpeed * Time.deltaTime;
            material.SetTextureOffset(m_mainTextureID, textureOffset);
        }
    }

    private Material FindAnimationMaterial ()
    {
        Material[] materials = beltRenderer.sharedMaterials;

        for (int i = 0, iEnd = materials.Length; i < iEnd; ++i)
        {
            if (materials[i].name == materialName)
            {
                return materials[i];
            }
        }

        return null;
    }
}

} // namespace EAC.Hytrol
