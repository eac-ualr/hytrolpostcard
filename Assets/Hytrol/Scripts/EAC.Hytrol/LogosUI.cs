
namespace EAC.Hytrol
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;
// local namespaces
using ObjectFinder = EAC.Utility.ObjectFinder;

public class LogosUI
    : MonoBehaviour
{
    private ApplicationManager m_applicationManager;
    private GameObject m_panelLogosGO;
    private Button m_btnUALittleRock;
    private Button m_btnEAC;
    private Button m_btnHytrol;

    private const string kURL_UALittleRock = "http://ualr.edu";
    private const string kURL_EAC = "http://eac-ualr.org";
    private const string kURL_Hytrol = "http://hytrol.com";
    private const string kURL_Modex = "http://modexshow.com";

        // ----------------------------------------------------------------------
        #region MonoBehaviour

        protected void Awake ()
    {
        m_applicationManager
            = ObjectFinder.FindObjectComponent<ApplicationManager>(ApplicationManager.objectName);
        Assert.IsNotNull(m_applicationManager);

        m_panelLogosGO = ObjectFinder.FindChild(gameObject, "Panel_Logos");
        Assert.IsNotNull(m_panelLogosGO);

        m_btnUALittleRock
            = ObjectFinder.GetChildComponent<Button>(gameObject, "Button_Logo_UALittleRock");
        Assert.IsNotNull(m_btnUALittleRock);

        m_btnEAC = ObjectFinder.GetChildComponent<Button>(gameObject, "Button_Logo_EAC");
        Assert.IsNotNull(m_btnEAC);

        m_btnHytrol = ObjectFinder.GetChildComponent<Button>(gameObject, "Button_Logo_Hytrol");
        Assert.IsNotNull(m_btnHytrol);

        if (m_applicationManager != null)
        {
            m_applicationManager.onModeChanged += OnApplicationModeChanged;
        }
    }

    protected void OnDestroy ()
    {
        if (m_applicationManager != null)
        {
            m_applicationManager.onModeChanged -= OnApplicationModeChanged;
        }
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void OnClickButtonUALittleRock ()
    {
        Application.OpenURL(kURL_UALittleRock);
    }

    public void OnClickButtonEAC ()
    {
        Application.OpenURL(kURL_EAC);
    }

    public void OnClickButtonHytrol ()
    {
        Application.OpenURL(kURL_Hytrol);
    }

    public void OnClickButtonModex()
    {
        Application.OpenURL(kURL_Modex);
    }

        // ----------------------------------------------------------------------

        private void OnApplicationModeChanged (
        ApplicationManager.Mode oldMode, ApplicationManager.Mode newMode)
    {
        if (newMode == ApplicationManager.Mode.TrackAugmentation)
        {
            m_panelLogosGO.SetActive(true);
        }
        else
        {
            m_panelLogosGO.SetActive(false);
        }
    }
}

} // namespace EAC.Hytrol
