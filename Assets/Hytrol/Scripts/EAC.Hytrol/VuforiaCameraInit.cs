
namespace EAC.Hytrol
{

// unity namespaces
using UnityEngine;
// external namespaces
using CameraDevice = Vuforia.CameraDevice;
using VuforiaARController = Vuforia.VuforiaARController;

public class VuforiaCameraInit
    : MonoBehaviour
{
    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        VuforiaARController controller = VuforiaARController.Instance;

        controller.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        controller.RegisterOnPauseCallback(OnVuforiaPaused);
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    private void OnVuforiaStarted ()
    {
        SetCameraFocusMode();
    }

    private void OnVuforiaPaused (bool paused)
    {
        if (!paused)
        {
            SetCameraFocusMode();
        }
    }

    private void SetCameraFocusMode ()
    {
        bool focusModeContinuousAuto = CameraDevice.Instance.SetFocusMode(
            CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);

        if (focusModeContinuousAuto)
            return;

        Debug.LogWarning(
            "[VuforiaCameraInit.SetCameraFocusMode] Could not enable FOCUS_MODE_CONTINUOUSAUTO.");

        bool focusModeNormal = CameraDevice.Instance.SetFocusMode(
            CameraDevice.FocusMode.FOCUS_MODE_NORMAL);

        if (focusModeNormal)
            return;

        Debug.LogWarning(
            "[VuforiaCameraInit.SetCameraFocusMode] Could not enable FOCUS_MODE_NORMAL.");
    }
}


} // namespace EAC.Hytrol
