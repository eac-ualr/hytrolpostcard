
namespace EAC.Hytrol
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;
// local namespaces
using ObjectFinder = EAC.Utility.ObjectFinder;


public class ShowHowToUI
    : MonoBehaviour
{
    private ApplicationManager m_applicationManager;
    private GameObject m_panelButtonsGO;

    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
         m_applicationManager
            = ObjectFinder.FindObjectComponent<ApplicationManager>(ApplicationManager.objectName);
        Assert.IsNotNull(m_applicationManager);

        m_panelButtonsGO = ObjectFinder.FindChild(gameObject, "Panel_Buttons_HowTo");
        Assert.IsNotNull(m_panelButtonsGO);

        if (m_applicationManager != null)
        {
            m_applicationManager.onModeChanged += OnApplicationModeChanged;
        }
    }

    protected void OnDestroy ()
    {
        if (m_applicationManager != null)
        {
            m_applicationManager.onModeChanged -= OnApplicationModeChanged;
        }
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void OnClickButtonShowHowTo ()
    {
        m_applicationManager.SwitchMode(ApplicationManager.Mode.DisplayInstructions);
    }

    // ----------------------------------------------------------------------

    private void OnApplicationModeChanged (
        ApplicationManager.Mode oldMode, ApplicationManager.Mode newMode)
    {
        if (newMode == ApplicationManager.Mode.TrackAugmentation)
        {
            m_panelButtonsGO.SetActive(true);
        }
        else
        {
            m_panelButtonsGO.SetActive(false);
        }
    }
}

} // namespace EAC.Hytrol
