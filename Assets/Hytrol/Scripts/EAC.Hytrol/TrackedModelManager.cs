
namespace EAC.Hytrol
{

// system namespaces
using System.Collections.Generic;
// unity namespaces
using UnityEngine;
using UnityEngine.Assertions;
// external namespaces
using Vuforia;


public class TrackedModelManager
    : MonoBehaviour
{
    private SweepSorterTrackableEventHandler m_eventHandler;
    private List<Renderer> m_renderers = new List<Renderer>();

    private void Awake ()
    {
        m_eventHandler = GetComponent<SweepSorterTrackableEventHandler>();
        Assert.IsNotNull(m_eventHandler);

        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        m_renderers.AddRange(renderers);

        // initially hide model
        SetRendererVisibility(false);
    }

    private void OnEnable ()
    {
        if (m_eventHandler != null)
        {
            m_eventHandler.onTrackingStateChanged += OnTrackingStateChanged;
        }
    }

    private void OnDisable ()
    {
        if (m_eventHandler != null)
        {
            m_eventHandler.onTrackingStateChanged -= OnTrackingStateChanged;
        }
    }

    private void OnTrackingStateChanged (bool tracking)
    {
        SetRendererVisibility(tracking);
    }

    private void SetRendererVisibility (bool visible)
    {
        for (int i = 0, iEnd = m_renderers.Count; i < iEnd; ++i)
        {
            m_renderers[i].enabled = visible;
        }
    }
}

} // namespace EAC.Hytrol
