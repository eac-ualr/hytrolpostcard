
namespace EAC.Hytrol
{

// unity namespaces
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine;
// local namespaces
using ObjectFinder = EAC.Utility.ObjectFinder;


public class HowToUI
    : MonoBehaviour
{
    private ApplicationManager m_applicationManager;

    private GameObject m_panelHowToGO;
    private Button m_btnClose;

    // URL used with version up to 1.0.5
    // private const string kURL_PostcardImage =
    //     "http://eac-ualr.org/HytrolProSortSS/ProSortSSAR.png";
    // URL used with version 1.1.0+
    private const string kURL_PostcardImage =
        "http://eac-ualr.org/HytrolProSortSS/ProSortSSAR_v10100.png";
    private bool isActive = false;
    // ----------------------------------------------------------------------
    #region MonoBehaviour

    protected void Awake ()
    {
        m_applicationManager
            = ObjectFinder.FindObjectComponent<ApplicationManager>(ApplicationManager.objectName);
        Assert.IsNotNull(m_applicationManager);

        m_panelHowToGO = ObjectFinder.FindChild(gameObject, "Panel");
        Assert.IsNotNull(m_panelHowToGO);

        m_btnClose = ObjectFinder.GetChildComponent<Button>(gameObject, "Button_Close");
        Assert.IsNotNull(m_btnClose);

        if (m_applicationManager != null)
        {
            m_applicationManager.onModeChanged += OnApplicationModeChanged;
        }
    }

    protected void Start()
    {

            if (PlayerPrefs.GetInt("HowToIsActive") == 1)
                OnClickButtonClose();

            PlayerPrefs.SetInt("HowToIsActive", 1);
    }

        protected void OnDestroy ()
    {
        if (m_applicationManager != null)
        {
            m_applicationManager.onModeChanged -= OnApplicationModeChanged;
        }
    }

    #endregion MonoBehaviour
    // ----------------------------------------------------------------------

    public void OnClickButtonClose ()
    {
        m_applicationManager.SwitchMode(ApplicationManager.Mode.TrackAugmentation);
    }

    public void OnClickButtonDownloadPostcardImage ()
    {
        Application.OpenURL(kURL_PostcardImage);
    }

    // ----------------------------------------------------------------------

    private void OnApplicationModeChanged (
        ApplicationManager.Mode oldMode, ApplicationManager.Mode newMode)
    {
        if (newMode == ApplicationManager.Mode.DisplayInstructions)
        {
            m_panelHowToGO.SetActive(true);
        }
        else
        {
            m_panelHowToGO.SetActive(false);
        }
    }
}

} // namespace EAC.Hytrol
