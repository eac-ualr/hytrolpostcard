Short Description
=================

Augmented Reality application showcasing the new Hytrol ProSort SS sorter


Long Description
================

Explore the benefits of the new Hytrol ProSort SS in this Augmented Reality application.  Visit Hytrol at our MODEX 2018 booth #B3219 and pick up your Hytrol Postcard, or download and print your own copy from http://eac-ualr.org/HytrolProSortSS/ProSortSSAR.png .  Then simply aim your device's camera at the postcard and experience an augmented reality demonstration of the newest member of Hytrol's ProSort family of sorting conveyor solutions.

For more information about Hytrol's versatile range of conveyor and sorter solutions visit our MODEX 2018 booth #B2319 or our website at http://hytrol.com

This augmented reality experience is developed in collaboration with the Emerging Analytics Center at the University of Arkansas at Little Rock.  For more information about this application and other projects in Virtual, Augmented Reality and Interactive Graphics visit us at http://eac-ualr.org



Apple Store Reviewer Notes
==========================

This is a Augmented Reality application that requires a target image for full functionality. The target image is attached, you can download it at http://eac-ualr.org/HytrolProSortSS/ProSortSSAR.png or from within the application using the "Download Postcard Image" button on the startup screen. A printed version of the image will be given to attendees at the MODEX 2018 trade show in Atlanta GA (https://www.modexshow.com).



Release Notes
=============
1.1.1
-----
- Fixed required Android API level

1.1.0
-----
- Added a second image target that triggers the scene

1.0.5
-----
- Adjust positioning of 3D model

1.0.4
-----
- Update target image
- Tweak "Download Postcard Image" button text size
